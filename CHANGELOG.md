# Changelog

---

### [1.9.1] - 2025-02-18

### amalthea.fileformats

- Fixed `FileFormat.opAssign()` signature. There was compilation problem with gdc.

### Other

- Fixed compilation of unit-tests.

- Small fixes in the build system.

---

## [1.9.0] - 2025-02-17

### amalthea.optional

- New library module with new data type `Optional`
  that either stores a value or doesn't store a value.

### amalthea.encoding

- Fixed a dangerous bug related to conversion of a string
  into a pointer to C-string in the `encodeText()` function.

- Fixed issues with signatures caused by incorrect use of `const`.

### amalthea.fs

- Fixed reading a directory containing inaccessible files.

### amalthea.fileformats

- Fixed reading of "complete_match" parameter from JSON file with formats.

### Other

- Important fixes in the build system.

---

## [1.8.0] - 2024-10-26

### amalthea.sys

- Added new functions: `findMountPoints()`, `findDevicesByMountPoint()`.
- Added functions for loop control: `findDevicesByFile()`, `getBackingFile()`.

### amalthea.dataprocessing

- Fixed a bug related to argument type matching in `isAmong()`.

---

## [1.7.1] - 2024-06-17

### amalthea.fileformats

- Advanced recognition logic: if signature lengths of format candidates are
  equal, the format with the most matching file extension is selected.
- Other minor changes.

---

## [1.7.0] - 2024-06-16

### amalthea.dataprocessing

- Added a new function `copyArray()`.
- Fixed bug related to constant correctness: now `calcIntersection()`,
  `calcUnion()`, `calcComplement()` takes constant arrays only.
  This solution violates backward compatibility.

### amalthea.fs

- Signatures of `isRegularFile()`, `isDir()`, `makeUnixFileModeLine()`
  have been changed. In cases where they takes a structure as an argument,
  the `in` argument attribute has been replaced by `const` and `ref const`.
  This solution violates backward compatibility.

### amalthea.sys

- Signal enumeration has become more cross-platform.
- `SIGSTKFLT` has been deleted.
- The `setupSignalHandler()` functions are deprecated.
  A new (similar) function `addHandlerSignal()` has been implemented.

### Other changes

- Resolved that using the `-preview=in` compilation flag for any library
  is flawed. This flag is disabled.

---

## [1.6.0] - 2024-01-24

### amalthea.sys

- Added `createMapping()` function to create file mappings
  and `MapMode` enumeration type for access modes of mappings.
- Added `getGroups()` function to get group list (all groups or groups by user id).

### amalthea.fs

- Added `applyFileAdvise()` function based on `posix_fadvise` and
  added `FileAdvice` enumeration.

### amalthea.dataprocessing

- Added `countElements()` and `divideByElemNumber()` functions.

### amalthea.csv

- Extensive corrections to the CSV structure behavior have been made.
- New exception class: `CsvException`.

### Other changes

- `in ref` attributes has replaced to `in`.
  Build uses `-preview=in` option for compilers.

---

## [1.5.0] - 2023-10-09

### Added

- New function `amalthea.sys.setupSignalHandler()` for UNIX signal processing.
- New function `amalthea.sys.getSignalDescription()` that returns string
  describing signal.
- UNIX signal enumeration.

---

## [1.4.0] - 2023-03-17

### Added

- Implemented an option for file formats that checks the complete match
  of the file with the requirements for the extension and signature.
- New function `truncate()` that changes a file size.
- New function: `getpagesize()` returns the number of bytes in a memory page.
- New function `safeMkdir()` creates a new directory only if it doesn't exist.
- Functions for memory mapping.

---

## [1.3.0] - 2022-10-20

### Added

- Functions for working with file extended attributes (xattr).
- Function `isSymlinkToDir()` checks if the file is a symbolic link
  to a directory.
- BSD-style exit codes for programs in the 'sys' module.

### Changed

- Small improvements of the code base.

---

## [1.2.1] - 2022-09-15

### Fixed

- Configuration file generation for pkg-config.

---

## [1.2.0] - 2022-09-13

### Added

- Functions mount()/umount() and loop device management functions.
- New file formats to the recognizing system.
- User can set custom paths to search for file format descriptions.
- Restored support of the GNU D compiler.
- Experimental build support based on Meson.

### Changed

- The project switched to dual licensing: BSL-1.0 or GNU LGPL-3.0+.

---

## [1.1.0] - 2022-01-30

### Added

- New function 'isEmptyDir'.

### Changed

- Minor changes in 'fs' and 'fileformats'.


---

## [1.0.1] - 2021-10-05

### Added

- Restored ddoc-macros for library documenation.

---

## [1.0] - 2021-10-04

### Added

- New functions for checking file types (isDir, isCharDevice and other).
- New function 'exists' of the 'fs' module.
- New function for getting Fibonacci numbers.
- Old functions for sorting associative arrays are deprecated,
  new functions created that return ordered arrays of tuples.

---

## [0.9.5] - 2021-07-29

### Fixed

- File format detection for empty file and for MP3.

---

## [0.9.4] - 2021-07-28

### Added

- The 'torrent' format in the file format recognizing system.

### Changed

- Minor changes in fileformats and build system.

---

## [0.9.3] - 2021-07-22

### Changed

- Fix a critical error in `statx_t`;
- Redone file format module:
  - formats are stored in json file instead of csv;
  - formats are loaded in runtime from "/etc/amalthea/fileformats.json"
    and "$HOME/.config/amalthea/\*.json";
  - fix format list;
  - new function 'contentTypeGuess' to detect MIME-type;
- Change project structure.

---

## [0.9.2] - 2021-07-14

### Added

- New functions in dataprocessing.d: getBinaryForm, stripLeft, stripRight.
- New function for creating hard links.

### Fixed

- Important fixes in file format recognition.

### Changed

- Other small improvements.

---

## [0.9.1] - 2021-03-21

### Changed

- Fixed getting HTTP headers in amalthea.net.getHeaders().
- Added new matrix template functions.
- Added a new module for working with time.
- FileStat uses a new syscall statx() instead of stat().
- getFiles and similar functions has a new parameter `saveAbsolutePath`.
- Deprecated functions removed.
- Minor general code improvements.

---

## [0.9.0] - 2021-01-16

### Changed

- Redesigned module for working with decimal numbers with fixed point:
    - now Decimal is based on BigInt;
    - decimal point position is set when creating a Decimal object.
- Improved the encoding module:
    - added Phobos-compatible encoding KOI8-U;
    - expanded transcoding capabilities in UniString;
    - added libiconv-based function for encoding transformation.
- The langlocal module has the ability to load translations from a CSV file.
- The dialog module was redesigned.
- The terminal module acquired a function for clearing screen.
- Made changes to the list of file formats.
- Updated samples.
- Build system was modified and simplified.
- New functions were created to view the contents of directories.
- Many functions are marked as deprecated.

---

## [0.8.1] - 2020-08-24

### Added

- The ability to build using dub.

---

## [0.8] - 2020-02-09

### Changed

- Added a new module for working with encodings.
- The 'amalthea.net' module is supplemented by working with sockets.
- Added a new function to get the content of HTML pages ('getPage').
- File format recognition improvements.

---

## [0.7.2] - 2019-12-31

### Changed

- Autogeneration of file format module was disabled.

---

## [0.7.1] - 2019-12-31

### Changed

- Minor data type corrections.

---

## [0.7] - 2019-12-30

### Changed

- filetypes.d renamed to fileformats.d.
- Structure FileType renamed to FileFormat.
- Modified generation of fileformats.d.
- Other improvements of building.
- Speeding up the function getSymlinksInfo().

---

## [0.6.2] - 2019-12-12

### Changed

- Build system: adaptation to RPM-based distributions.

---

## [0.6.1] - 2019-12-06

### Changed

- Build system: modified creating of tarball.

---

## [0.6] - 2019-12-06

- Generating of deb and rpm has been removed from the build system.
- New math module added.
- Some problems fixed.

---

## [0.5.5] - 2019-10-27

### Fixed

- Fixed code related to the compiler version.

---

## [0.5.4] - 2019-10-27

### Changed

- README.md: the list of supported distributions has been changed.

---

## [0.5.2] - 2019-10-26

### Changed

- Generation of deb and rpm has been removed from the build system.
- Small changes associated with changes in the D language.

---

## [0.5] - 2019-06-17

### Changed

- The PostPhobos library renamed to Amalthea.
- There was a module for determining file types.
- The `crypto` module has received a slight development (added Stribog).
- Small general improvements.

---

## [0.3.13] - 2019-04-03

### Changed

- Building system has been changed.
- Modules `decimal` and `matrix_calc` added.

