#!/bin/bash
################################################################################
# This file is distributed under the GNU All-Permissive License.
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.
# This file is offered as-is, without any warranty.
################################################################################

# machine architecture: "x86_64-linux-gnu", "arm-linux-gnueabihf", etc.
MACHINE=$(gcc -dumpmachine)

# ldc2, gdc, dmd
DC=$1
# include-path, library-path, debug-flags, release-flags, shared-flags
REQUEST=$2
# soname for shared library
SONAME=$3


# Receives a sequence of elements and one value (last argument) for comparison,
# prints TRUE if the value is found, else FALSE
Can_Find() {
    elem="${@:$#}"
    for arg in ${@:1:$#-1}; do
        if [[ "$elem" == "$arg" ]]; then
            echo TRUE
            return
        fi
    done
    echo FALSE
}


Check_Compiler() {
    COMPILER_PATH=$(which $DC)
    if [[ -z "$COMPILER_PATH" ]]; then
        echo "${DC}: compiler not found. Exit..."
        exit 1
    fi
    if [[ $DC == gdc ]]; then
        GDC_VERSION=`gdc -dumpversion`
    fi
}
Check_Compiler


# Creates DISTR_MODE (equal to DEB, RPM, ARCH of DEFAULT)
Define_Distr_Mode() {
    INFOFILE="/etc/os-release"
    if [[ ! -e /etc/os-release ]]; then
        DISTR_MODE=DEFAULT
        return
    fi

    ID=$(cat "$INFOFILE" | grep -e "^ID=" | cut -c4- | sed 's/"//g')
    ID_LIKE=$(cat "$INFOFILE" | grep -e "^ID_LIKE=" | cut -c9- | sed 's/"//g')
    read -ra ID_LIKE_ARR <<< "$ID_LIKE"
    DISTR_MODE=""
    if [[ "$ID" == debian ]]; then
        DISTR_MODE=DEB
    elif [[ "$ID" == arch ]]; then
        DISTR_MODE=ARCH
    elif [[ "$ID" == fedora ]]; then
        DISTR_MODE=RPM
    fi

    # ID_LIKE can contain "manjaro arch" or "opensuse suse"
    if [[ `Can_Find ${ID_LIKE_ARR[@]} debian` == TRUE ]]; then
        DISTR_MODE=DEB
    elif [[ `Can_Find ${ID_LIKE_ARR[@]} arch` == TRUE ]]; then
        DISTR_MODE=ARCH
    elif [[ `Can_Find ${ID_LIKE_ARR[@]} suse` == TRUE ]]; then
        DISTR_MODE=RPM
    elif [[ `Can_Find ${ID_LIKE_ARR[@]} fedora` == TRUE ]]; then
        DISTR_MODE=RPM
    fi

    if [[ -z $DISTR_MODE ]]; then
        DISTR_MODE=DEFAULT
    fi
}

Define_Distr_Mode  # create DISTR_MODE


# Library directory
DEFAULT_LIB_DIR=lib
DEB_LIB_DIR=lib/${MACHINE}
if [[ `getconf LONG_BIT` == 64 ]]; then
    RPM_LIB_DIR=lib64
else
    RPM_LIB_DIR=lib
fi

# ldc2 paths
DEFAULT_LDC2_INC_DIR="include/d"
DEB_LDC2_INC_DIR="lib/ldc/${MACHINE}/include/d"  # Debian, Ubuntu
RPM_LDC2_INC_DIR=$DEFAULT_LDC2_INC_DIR           # Fedora, openSUSE
ARCH_LDC2_INC_DIR="include/dlang/ldc"            # ArchLinux, Manjaro
# ldc2 flags
LDC2_DEBUG_OPTIONS="-d-debug --gc"
LDC2_SHLIB_OPTIONS="-shared -soname ${SONAME}"
LDC2_OPTIM_OPTIONS="-O -release --gc"
LDC2_EXTERNAL="-L-lgio-2.0 -L-lglib-2.0"
LDC2_LPATH_OPT="-L-L"
LDC2_LNAME_OPT="-L-l"

# dmd paths
DEFAULT_DMD_INC_DIR="include/dmd"
# dmd flags
DMD_DEBUG_OPTIONS="-debug -g"
DMD_SHLIB_OPTIONS="-fPIC -shared -L-soname=${SONAME}"
DMD_OPTIM_OPTIONS="-O -release"
DMD_EXTERNAL="-L-lgio-2.0 -L-lglib-2.0"
DMD_LPATH_OPT="-L-L"
DMD_LNAME_OPT="-L-l"

# gdc paths
DEFAULT_GDC_INC_DIR="lib/gcc/${MACHINE}/${GDC_VERSION}/include/d"
RPM_GDC_INC_DIR="${DEFAULT_GDC_INC_DIR}"
if [[ `Can_Find ${ID_LIKE_ARR[@]} suse` == TRUE ]]; then
    RPM_GDC_INC_DIR="${RPM_LIB_DIR}/gcc/${MACHINE}/${GDC_VERSION}/include/d"
fi

# gdc flags
GDC_BASE_OPTIONS="-fPIC"
GDC_DEBUG_OPTIONS="-fdebug ${GDC_BASE_OPTIONS}"
GDC_SHLIB_OPTIONS="-shared -Wl,-soname,${SONAME} ${GDC_BASE_OPTIONS}"
GDC_OPTIM_OPTIONS="-O2 -frelease ${GDC_BASE_OPTIONS}"
GDC_EXTERNAL="-lgio-2.0 -lglib-2.0"
GDC_LPATH_OPT="-L"
GDC_LNAME_OPT="-l"


Print_Inc_Dir() {
    INC_DIR=$(eval echo \$${DISTR_MODE}_${DC^^}_INC_DIR)
    if [[ -z ${INC_DIR} ]]; then
        INC_DIR=$(eval echo \$DEFAULT_${DC^^}_INC_DIR)
    fi
    echo ${INC_DIR}
}


Print_Lib_Dir() {
    LIB_DIR="$(eval echo \$${DISTR_MODE}_LIB_DIR)"
    if [[ -z ${LIB_DIR} ]]; then
        LIB_DIR="${DEFAULT_LIB_DIR}"
    fi
    echo ${LIB_DIR}
}


Print_Flags() {
    FLAGS=""
    if [[ ${REQUEST} == "shared-flags" ]]; then
        FLAGS=$(eval echo \$${DC^^}_SHLIB_OPTIONS)
    elif [[ ${REQUEST} == "external-libs" ]]; then
        FLAGS=$(eval echo \$${DC^^}_EXTERNAL)
    elif [[ ${REQUEST} == "debug-flags" ]]; then
        FLAGS=$(eval echo \$${DC^^}_DEBUG_OPTIONS)
    elif [[ ${REQUEST} == "release-flags" ]]; then
        FLAGS=$(eval echo \$${DC^^}_OPTIM_OPTIONS)
    elif [[ ${REQUEST} == "libpath-opt" ]]; then
        FLAGS=$(eval echo \$${DC^^}_LPATH_OPT)
    elif [[ ${REQUEST} == "libname-opt" ]]; then
        FLAGS=$(eval echo \$${DC^^}_LNAME_OPT)
    fi
    echo "${FLAGS}"
}


if [[ $REQUEST == "include-path" ]]; then
    Print_Inc_Dir
elif [[ $REQUEST == "library-path" ]]; then
    Print_Lib_Dir
else
    Print_Flags
fi
