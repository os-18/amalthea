The Amalthea Library is free software.
This library is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.


**Official repository**: [https://codeberg.org/os-18/amalthea](https://codeberg.org/os-18/amalthea)  

**Mirror:** [https://gitlab.com/os-18/amalthea](https://gitlab.com/os-18/amalthea)

---

## Amalthea

Amalthea is a small general Linux-specific library for the D programming language.
It provides few useful functions.

**Library documentation:**  
[https://os-18.codeberg.page/amalthea/](https://os-18.codeberg.page/amalthea/)

---

## Ready-made packages

See:  
[Download Page of OS-18](https://codeberg.org/os-18/os-18-docs/src/branch/master/OS-18_Packages.md)  

The page contains information about installing several packages, including Amalthea.

---

## Build from source

### Preparing

You must have at least one of these compilers:

- [Digital Mars D Compiler](https://dlang.org/download.html#dmd)
- [LLVM D Compiler](https://wiki.dlang.org/LDC)
- [GNU D Compiler](https://www.gdcproject.org)

Also the library needs **libgio-2.0** and **libglib-2.0**.
Most likely, these libraries are already on your system.


### Compilation and installation

Building static and dynamic-link libraries for the **ldc2** compiler (by default):

```bash
make
```

Installation:

```bash
make install
```

After that, the library for ldc2 is ready for use.

You can install the library to any other directory:

```bash
make install DESTDIR=/home/$USER/sandbox
```

Uninstalling:

```bash
make uninstall
```

If you installed to an alternative directory:

```bash
make uninstall DESTDIR=/home/$USER/sandbox
```

### Other compilers

Compiler by default is **ldc2**.  
You can choose other compiler (for example, **dmd**):

```bash
make DC=dmd
```

Installing:

```bash
make install DC=dmd
```

Uninstalling:

```bash
make uninstall DC=dmd
```

---


## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---
