/*
 * This file is part of the Amalthea library.
 * Copyright (C) 2018-2021, 2024 Eugene 'Vindex' Stulin
 * Distributed under the BSL 1.0 or the GNU LGPL 3.0 or later.
 */

/**
 * Some useful functions based on std.net.curl.
 */
module amalthea.net;

public import amalthea.libcore;

import amalthea.dataprocessing;
import amalthea.encoding;
import amalthea.fs;

import std.algorithm, std.format, std.range, std.regex, std.string;
import std.net.curl;

static alias stdGet  = std.net.curl.get;
static alias stdPost = std.net.curl.post;

alias urlEncode = amalthea.dataprocessing.urlEncode;

version (DigitalMars)
alias StdNetException = std.net.curl.HTTPStatusException;
else
alias StdNetException = std.net.curl.CurlException;


/*******************************************************************************
 * Exception for common errors in this module.
 */
class AmaltheaNetException : Exception {
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}


/*******************************************************************************
 * GET request to raw content.
 * Returns: Byte array.
 */
ubyte[] getRaw(string url) {
    auto client = HTTP();
    client.verifyPeer = false;
    client.method = HTTP.Method.get;
    client.url = url;
    HTTP.StatusLine statusLine;
    import std.array : appender;
    auto content = appender!(ubyte[])();
    client.onReceive = (ubyte[] data) {
        content ~= data;
        return data.length;
    };
    client.onReceiveStatusLine = (HTTP.StatusLine l) { statusLine = l; };
    client.perform();
    if (statusLine.code / 100 != 2) {
        string exceptionMsg = format(
            "HTTP request returned status code %d (%s)",
            statusLine.code, statusLine.reason
        );
        throw new HTTPStatusException(statusLine.code, exceptionMsg);
    }
    return content.data;
}


/*******************************************************************************
 * POST request.
 */
T[] post(T = char)(
    const(char)[] url, string[string] postDict, HTTP conn = HTTP()
) if (is(T == char) || is(T == ubyte)) {
    return stdPost!T(url, urlEncode(postDict), conn);
}


/*******************************************************************************
 * POST request to get an answer with raw content.
 * Returns: Byte array.
 */
ubyte[] postRaw(const(char)[] url, string[string] postDict, HTTP c = HTTP()) {
    return amalthea.net.post!ubyte(url, postDict, c);
}


/*******************************************************************************
 * Gets HTTP response headers by URL.
 */
string[string] getHeaders(in char[] url) {
    auto http = HTTP(url);
    http.perform;
    return http.responseHeaders;
}


/*******************************************************************************
 * Gets HTTP content type by URL.
 */
string getContentType(in char[] url) {
    auto header = getHeaders(url);
    string contentType;
    foreach(key, value; header) {
        if (key.toLower == "content-type") {
            contentType = value;
            break;
        }
    }
    return contentType;
}


/*******************************************************************************
 * Gets content charset (possibly empty) by URL.
 */
string getCharset(const(char)[] url) {
    string contentType = getContentType(url);
    return extractCharsetFromContentType(contentType);
}


/*******************************************************************************
 * Checks if the URL is a link to HTML page.
 */
bool isLinkToHTML(string url) {
    try {
        string contentType = getContentType(url);
        if (contentType.canFind("text/html")) {
            return true;
        }
    } catch(Exception e) {
        return false;
    }
    return false;
}


/*******************************************************************************
 * Get text content as amalthea.encoding.UniString by URL.
 * The implementation of 'get' from the standard library is taken as a basis.
 */
UniString getPage(string url) {
    auto client = HTTP();
    client.verifyPeer = false;
    client.method = HTTP.Method.get;
    client.url = url;
    HTTP.StatusLine statusLine;
    import std.array : appender;
    auto content = appender!(ubyte[])();
    client.onReceive = (ubyte[] data) {
        content ~= data;
        return data.length;
    };
    string contentType;
    string charset;
    client.onReceiveHeader = (in char[] key,
                              in char[] value) {
        auto lowerKey = key.idup.toLower;
        if (lowerKey == "content-length") {
            import std.conv : to;
            content.reserve(value.to!size_t);
        } else if (lowerKey == "content-type") {
            contentType = value.idup;
            charset = extractCharsetFromContentType(contentType);
        }
    };
    client.onReceiveStatusLine = (HTTP.StatusLine l) { statusLine = l; };
    client.perform();

    if (statusLine.code / 100 != 2) {
        string exceptionMsg = format(
            "HTTP request returned status code %d (%s)",
            statusLine.code, statusLine.reason
        );
        throw new HTTPStatusException(statusLine.code, exceptionMsg);
    }
    if (charset.empty) {
        charset = "UTF-8";
    }
    auto page = UniString(charset, content.data);
    return page;
}



/*******************************************************************************
 * This function searches all elements from HTML page with a specific tag.
 */
auto getElementsByTag(string html, string tag) {
    return getElementsByTagAndAttribute(html, tag);
}


/*******************************************************************************
 * This function searches all elements by a specific tag and an attribute.
 * Returns: Page element info (tag name and possible attributes)
 *          and content of the element as tuple with two elements.
 */
auto getElementsByTagAndAttribute(
    string html,
    string tag,
    string attrName = "",
    string attrValue = ""
) {
    html = html.replace("\n", " ");
    auto lowerTag = tag.toLower;
    auto upperTag = tag.toUpper;
    html = html.replace("<"~upperTag~" ", "\n<"~lowerTag~" ")
               .replace("<"~upperTag~">", "\n<"~lowerTag~">")
               .replace("</"~upperTag~" ", "</"~lowerTag~" \n")
               .replace("</"~upperTag~">", "</"~lowerTag~">\n");
    html = html.replace("<"~lowerTag~" ", "\n<"~lowerTag~" ")
               .replace("<"~lowerTag~">", "\n<"~lowerTag~">")
               .replace("</"~lowerTag~" ", "</"~lowerTag~" \n")
               .replace("</"~lowerTag~">", "</"~lowerTag~">\n");
    if (!attrValue.empty) attrValue = ` *= *"?` ~ attrValue ~ `"?`;
    string openingTag, closingTag;
    openingTag = attrName.empty ? tag : tag~" ";
    closingTag = tag;
    string e;
    e = format!`^<(?P<declaration>%s[^>]*%s%s *[^>]*)>(?P<content>.*)</%s *>$`
        (openingTag, attrName, attrValue, closingTag);
    bool pairedTag = true;
    auto r = regex(e, "im");
    if (count(matchAll(html, r)) == 0) {
        e = format!`^<(?P<declaration>%s [^>]*%s%s *[^>]*)>`
                   (tag, attrName, attrValue);
        pairedTag = false;
        r = regex(e, "im");
    }
    Tuple!(string, "declaration", string, "content")[] elements;
    foreach(c; matchAll(html, r)) {
        auto decl = c["declaration"].replace("\t", " ");
        decl = decl.removeDuplicateConsecutiveSubstring(" ");
        elements ~= Tuple!(string, "declaration", string, "content")
                          (decl, pairedTag ? c["content"] : "");
    }
    return elements;
}


/*******************************************************************************
 * This function returns title of Internet-page.
 */
string getHTMLPageTitle(string address) {
    string html = getPage(address).toString;
    html.replaceSpecialMnemonics;
    auto res = getElementsByTag(html, "title");
    if (res.empty) return "";
    return res[0].content;
}


/*******************************************************************************
 * Search and replace special characters in HTML for normal view.
 */
ref string replaceSpecialMnemonics(return ref string HTMLText) {
    import std.string;
    string[string] specialHTMLSymbols = [
        "&iexcl;"    : "¡",
        "&cent;"     : "¢",
        "&pound;"    : "£",
        "&curren;"   : "¤",
        "&yen;"      : "¥",
        "&brvbar;"   : "¦",
        "&sect;"     : "§",
        "&uml;"      : "¨",
        "&copy;"     : "©",
        "&ordf;"     : "ª",
        "&laquo;"    : "«",
        "&raquo;"    : "»",
        "&not;"      : "¬",
        "&shy;"      : "­",
        "&reg;"      : "®",
        "&macr;"     : "¯",
        "&deg;"      : "°",
        "&plusmn;"   : "±",
        "&sup2;"     : "²",
        "&sup3;"     : "³",
        "&acute;"    : "´",
        "&micro;"    : "µ",
        "&para;"     : "¶",
        "&middot;"   : "·",
        "&cedil;"    : "¸",
        "&sup1;"     : "¹",
        "&ordm;"     : "º",
        "&frac14;"   : "¼",
        "&frac12;"   : "½",
        "&frac34;"   : "¾",
        "&iquest;"   : "¿",
        "&Agrave;"   : "À",
        "&Aacute;"   : "Á",
        "&Acirc;"    : "Â",
        "&Atilde;"   : "Ã",
        "&Auml;"     : "Ä",
        "&Aring;"    : "Å",
        "&AElig;"    : "Æ",
        "&Ccedil;"   : "Ç",
        "&Egrave;"   : "È",
        "&Eacute;"   : "É",
        "&Ecirc;"    : "Ê",
        "&Euml;"     : "Ë",
        "&Igrave;"   : "Ì",
        "&Iacute;"   : "Í",
        "&Icirc;"    : "Î",
        "&Iuml;"     : "Ï",
        "&ETH;"      : "Ð",
        "&Ntilde;"   : "Ñ",
        "&Ograve;"   : "Ò",
        "&Oacute;"   : "Ó",
        "&Ocirc;"    : "Ô",
        "&Otilde;"   : "Õ",
        "&Ouml;"     : "Ö",
        "&times;"    : "×",
        "&Oslash;"   : "Ø",
        "&Ugrave;"   : "Ù",
        "&Uacute;"   : "Ú",
        "&Ucirc;"    : "Û",
        "&Uuml;"     : "Ü",
        "&Yacute;"   : "Ý",
        "&THORN;"    : "Þ",
        "&szlig;"    : "ß",
        "&agrave;"   : "à",
        "&aacute;"   : "á",
        "&acirc;"    : "â",
        "&atilde;"   : "ã",
        "&auml;"     : "ä",
        "&aring;"    : "å",
        "&aelig;"    : "æ",
        "&ccedil;"   : "ç",
        "&egrave;"   : "è",
        "&eacute;"   : "é",
        "&ecirc;"    : "ê",
        "&euml;"     : "ë",
        "&igrave;"   : "ì",
        "&iacute;"   : "í",
        "&icirc;"    : "î",
        "&iuml;"     : "ï",
        "&eth;"      : "ð",
        "&ntilde;"   : "ñ",
        "&ograve;"   : "ò",
        "&oacute;"   : "ó",
        "&ocirc;"    : "ô",
        "&otilde;"   : "õ",
        "&ouml;"     : "ö",
        "&divide;"   : "÷",
        "&oslash;"   : "ø",
        "&ugrave;"   : "ù",
        "&uacute;"   : "ú",
        "&ucirc;"    : "û",
        "&uuml;"     : "ü",
        "&yacute;"   : "ý",
        "&thorn;"    : "þ",
        "&yuml;"     : "ÿ",
        "&fnof;"     : "ƒ",
        "&Alpha;"    : "Α",
        "&Beta;"     : "Β",
        "&Gamma;"    : "Γ",
        "&Delta;"    : "Δ",
        "&Epsilon;"  : "Ε",
        "&Zeta;"     : "Ζ",
        "&Eta;"      : "Η",
        "&Theta;"    : "Θ",
        "&Iota;"     : "Ι",
        "&Kappa;"    : "Κ",
        "&Lambda;"   : "Λ",
        "&Mu;"       : "Μ",
        "&Nu;"       : "Ν",
        "&Xi;"       : "Ξ",
        "&Omicron;"  : "Ο",
        "&Pi;"       : "Π",
        "&Rho;"      : "Ρ",
        "&Sigma;"    : "Σ",
        "&Tau;"      : "Τ",
        "&Upsilon;"  : "Υ",
        "&Phi;"      : "Φ",
        "&Chi;"      : "Χ",
        "&Psi;"      : "Ψ",
        "&Omega;"    : "Ω",
        "&alpha;"    : "α",
        "&beta;"     : "β",
        "&gamma;"    : "γ",
        "&delta;"    : "δ",
        "&epsilon;"  : "ε",
        "&zeta;"     : "ζ",
        "&eta;"      : "η",
        "&theta;"    : "θ",
        "&iota;"     : "ι",
        "&kappa;"    : "κ",
        "&lambda;"   : "λ",
        "&mu;"       : "μ",
        "&nu;"       : "ν",
        "&xi;"       : "ξ",
        "&omicron;"  : "ο",
        "&pi;"       : "π",
        "&rho;"      : "ρ",
        "&sigmaf;"   : "ς",
        "&sigma;"    : "σ",
        "&tau;"      : "τ",
        "&upsilon;"  : "υ",
        "&phi;"      : "φ",
        "&chi;"      : "χ",
        "&psi;"      : "ψ",
        "&omega;"    : "ω",
        "&thetasym;" : "ϑ",
        "&upsih;"    : "ϒ",
        "&piv;"      : "ϖ",
        "&bull;"     : "•",
        "&hellip;"   : "…",
        "&prime;"    : "′",
        "&Prime;"    : "″",
        "&oline;"    : "‾",
        "&frasl;"    : "⁄",
        "&weierp;"   : "℘",
        "&image;"    : "ℑ",
        "&real;"     : "ℜ",
        "&trade;"    : "™",
        "&alefsym;"  : "ℵ",
        "&larr;"     : "←",
        "&uarr;"     : "↑",
        "&rarr;"     : "→",
        "&darr;"     : "↓",
        "&harr;"     : "↔",
        "&crarr;"    : "↵",
        "&lArr;"     : "⇐",
        "&uArr;"     : "⇑",
        "&rArr;"     : "⇒",
        "&dArr;"     : "⇓",
        "&hArr;"     : "⇔",
        "&forall;"   : "∀",
        "&part;"     : "∂",
        "&exist;"    : "∃",
        "&empty;"    : "∅",
        "&nabla;"    : "∇",
        "&isin;"     : "∈",
        "&notin;"    : "∉",
        "&ni;"       : "∋",
        "&prod;"     : "∏",
        "&sum;"      : "∑",
        "&minus;"    : "−",
        "&lowast;"   : "∗",
        "&radic;"    : "√",
        "&prop;"     : "∝",
        "&infin;"    : "∞",
        "&ang;"      : "∠",
        "&and;"      : "∧",
        "&or;"       : "∨",
        "&cap;"      : "∩",
        "&cup;"      : "∪",
        "&int;"      : "∫",
        "&there4;"   : "∴",
        "&sim;"      : "∼",
        "&cong;"     : "≅",
        "&asymp;"    : "≈",
        "&ne;"       : "≠",
        "&equiv;"    : "≡",
        "&le;"       : "≤",
        "&ge;"       : "≥",
        "&sub;"      : "⊂",
        "&sup;"      : "⊃",
        "&nsub;"     : "⊄",
        "&sube;"     : "⊆",
        "&supe;"     : "⊇",
        "&oplus;"    : "⊕",
        "&otimes;"   : "⊗",
        "&perp;"     : "⊥",
        "&sdot;"     : "⋅",
        "&lceil;"    : "⌈",
        "&rceil;"    : "⌉",
        "&lfloor;"   : "⌊",
        "&rfloor;"   : "⌋",
        "&lang;"     : "〈",
        "&rang;"     : "〉",
        "&loz;"      : "◊",
        "&spades;"   : "♠",
        "&clubs;"    : "♣",
        "&hearts;"   : "♥",
        "&diams;"    : "♦",

        "&apos;"     : "'",
        "&quot;"     : "\"",
        "&amp;"      : "&",
        "&lt;"       : "<",
        "&gt;"       : ">",
        "&OElig;"    : "Œ",
        "&oelig;"    : "œ",
        "&Scaron;"   : "Š",
        "&scaron;"   : "š",
        "&Yuml;"     : "Ÿ",
        "&circ;"     : "ˆ",
        "&tilde;"    : "˜",
        "&ndash;"    : "–",
        "&mdash;"    : "—",
        "&lsquo;"    : "‘",
        "&rsquo;"    : "’",
        "&sbquo;"    : "‚",
        "&ldquo;"    : "“",
        "&rdquo;"    : "”",
        "&bdquo;"    : "„",
        "&dagger;"   : "†",
        "&Dagger;"   : "‡",
        "&permil;"   : "‰",
        "&lsaquo;"   : "‹",
        "&rsaquo;"   : "›",
        "&euro;"     : "€"
    ];
    foreach(k, v; specialHTMLSymbols) {
        HTMLText = HTMLText.replace(k, v);
    }
    return HTMLText;
}


private string extractCharsetFromContentType(string contentType) {
    string charset;
    if (!contentType.empty) {
        auto fields = contentType.split(';');
        foreach(field; fields) {
            field = field.strip;
            if (field.startsWith("charset=")) {
                charset = field["charset=".length .. $].idup;
                break;
            }
        }
    }
    return charset;
}

