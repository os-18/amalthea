/* This file is part of the Amalthea library.
 * Copyright (C) 2018-2025 Eugene 'Vindex' Stulin
 * Distributed under the BSL 1.0 or the GNU LGPL 3.0 or later.
 */

module amalthea;

public import
    amalthea.libcore,
    amalthea.crypto,
    amalthea.csv,
    amalthea.dataprocessing,
    amalthea.decimal,
    amalthea.dialog,
    amalthea.encoding,
    amalthea.fileformats,
    amalthea.fs,
    amalthea.langlocal,
    amalthea.math,
    amalthea.matrixmath,
    amalthea.net,
    amalthea.optional,
    amalthea.sys,
    amalthea.terminal,
    amalthea.time;
