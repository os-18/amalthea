#!/usr/bin/rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

import amalthea.langlocal;
import std.stdio;

void main() {
    initLocalization("translations.csv");
    writeln(s_("Hello")); // Hello
    chooseLanguage("ru_RU");
    "Hello".s_.writeln; // Привет
    chooseLanguage("eo");
    "Hello".s_.writeln; // Saluton
}
