#!/usr/bin/env -S rdmd --compiler=ldc2 -J.

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

import amalthea.langlocal;
import std.csv, std.stdio;

alias Record = Tuple!(string, string, string);

static immutable string[][] table = () {
    string[][] t;
    string csvText = import("translations.csv");

    foreach (record; csvReader!(Record)(csvText)) {
        t ~= [record[0], record[1], record[2]];
    }

    return t;
}();

pragma(msg, table);

void main() {
    initLocalization(table, "en_US");
    writeln(s_("Hello"));  // Hello
    chooseLanguage("ru_RU");
    "Hello".s_.writeln;  // Привет
    chooseLanguage("eo");
    "Hello".s_.writeln;  // Saluton
}
