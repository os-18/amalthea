#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

import amalthea.fs;

int main(string[] args) {
    string source = args[1];
    string destination = args[2];
    amalthea.fs.cp(source, destination, No.followLinks, No.passErrors);
    return 0;
}
