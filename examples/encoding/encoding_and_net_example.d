#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

import std.encoding;

import amalthea.dataprocessing,
       amalthea.net,
       amalthea.encoding;

void main() {
    string utfText;

    string linkToKOI8R = "https://www.opennet.ru/opennews/art.shtml?num=52171";
    auto koi8rSample = cast(KOI8RString)getRaw(linkToKOI8R);
    writeln("KOI8R-encoded page was received.");
    amalthea.encoding.transcode(koi8rSample, utfText);
    File("koi8r_to_utf8.html", "w").write(utfText);
    writeln("koi8r_to_utf8.html was written.");
    writeln("-");

    string linkToKOI8U = "https://mirror.yandex.ru/mirrors/ftp.linux.kiev.ua/"
                       ~ "docs/developer/libs/ncurses.html";
    auto koi8uSample = cast(KOI8UString)getRaw(linkToKOI8U);
    writeln("KOI8U-encoded page was received.");
    amalthea.encoding.transcode(koi8uSample, utfText);
    File("koi8u_to_utf8.html", "w").write(utfText);
    writeln("koi8u_to_utf8.html was written.");
    writeln("-");

    // alternative method to get encoded html page
    UniString page = getPage(linkToKOI8R);
    writeln(page.getEncodingName()); //KOI8-R
    // we can get the same string in new encoding
    auto win1251Page = page.toEncodedString!Windows1251String;
    std.encoding.transcode(win1251Page, utfText);
    File("win1251_to_utf8.html", "w").write(utfText);
    writeln("win1251_to_utf8.html was written.");
    writeln("-");
}
