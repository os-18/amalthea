#!/usr/bin/rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

module net_sample;

import amalthea.net;
import std.stdio;

import std.datetime, std.range, core.stdc.time;

void main(string[] args) {
    auto urls = [
        "https://dlang.org",
        "https://habrahabr.ru/post/352950/",
        "http://www.objs.com/survey/OFSExt.htm",
        "http://samag.ru/archive/article/3613",
        "http://striver00.ru/D/",
        "http://lhs-blog.info/topics/programming/dlang/",
        "https://4pda.ru/forum/index.php?showtopic=383300"
    ];
    foreach(url; urls) {
        try {
            string title = getHTMLPageTitle(url);
            writeln("URL:     ", url);
            writeln("Title:   ", title);
            auto charset = getCharset(url);
            writeln("Charset: ", charset.empty ? "(unknown)" : charset);
            writeln("--------------------------------------------------------");
        } catch (Exception e) {
            writeln("Error with ", url);
            writeln(e.msg);
            writeln("--------------------------------------------------------");
        }
    }
}
