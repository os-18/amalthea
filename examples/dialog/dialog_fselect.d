#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

import std.process;
import std.datetime, std.file, std.format;

import amalthea.dataprocessing,
       amalthea.dialog;
import amalthea.terminal;
import amalthea.sys : getTimeString;

void runMsgBox(string f) {
    if (f == "") {
        return;
    }
    SysTime accessTime, modificationTime;
    getTimes(f, accessTime, modificationTime);
    ulong size = getSize(f);
    string modTime, accTime;
    modTime = getTimeString(modificationTime);
    accTime = getTimeString(accessTime);
    string fmt = "%s\nSize: %d B\nModification: %s\nLast access : %s";
    string fileInfo = format(fmt, f, size, modTime, accTime);
    auto msgbox = new MsgBox("", fileInfo, 9, 56);
    msgbox.run();
}


void runFSelect() {
    auto box = new FSelect("Welcome");
    box.run();
    runMsgBox(box.getPath());
}


void main() {
    Dialog.setBackTitle("File viewer");
    scope(exit) clearScreen();

    runFSelect();
}

