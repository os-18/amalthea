#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

import std.algorithm, std.array, std.process;

import amalthea.dataprocessing,
       amalthea.dialog;
import amalthea.dataprocessing : stripRight;
import amalthea.terminal;

void runYesNo() {
    auto yesno = new YesNo("Question", "Do you want to finish?");
    yesno.setButtonHandler(DialogAction.YES, { runInfoBox("Good bye!"); });
    yesno.setButtonHandler(DialogAction.NO,  { yesno.run(); });
    yesno.run();
}


void runMsgBox() {
    auto msgbox = new MsgBox("Title", "This is an experimental script");
    msgbox.run();
}


void runInputBox() {
    auto box = new InputBox("Input", "Enter your name:");
    box.run();
    string greeting = "Hello, " ~ box.getEntry() ~ "!";
    runInfoBox(greeting, 2);
}


void runInfoBox(string text, uint sleepTime=1) {
    auto infobox = new InfoBox("Info", text);
    infobox.setSleepTime(sleepTime);
    infobox.run();
}


void runCheckList() {
    auto checklist = new CheckList("", "Color combination", 8);
    string[string] items = [
        "1":"Maroon", "2":"Yellow", "3":"Dark green", "4":"Black"
    ];
    foreach(number; items.keys.sort) {
        checklist.addItem(number, items[number]);
    }
    checklist.run();
    string[] selected = checklist.getSelectedItems();
    string message;
    foreach(number; selected) {
        message ~= items[number] ~ "/";
    }
    if (message == "") {
        message = "It's an interesting choise.";
    } else {
        message = message.stripRight('/') ~ ".\nIt's an interesting choise.";
    }
    runInfoBox(message, 3);
}


void runRadioList() {
    auto radiolist = new RadioList("", "Choose our planet:", 10, 64);
    radiolist.addItem("Mercury", "The smallest and hottest planet");
    radiolist.addItem("Venus", "The second planet from the Sun");
    radiolist.addItem("Earth", "A planet with a nitrogen-oxygen atmosphere");
    radiolist.addItem("Mars", "Deserted red planet");
    radiolist.addItem("Jupiter", "The largest planet in the Solar System");
    radiolist.run();
    string selected = radiolist.getSelectedItem();
    runInfoBox(selected ~ " is a good planet.", 3);
}


void runMenu() {
    void showMessage(string title, string text) {
        auto msgbox = new MsgBox(title, text, 12, 54);
        msgbox.run();
    }
    auto menu = new Menu("Licencies", "Show information about", 12);
    auto mit = "The MIT License is a permissive free software license " ~
"originating at the Massachusetts Institute of Technology (MIT) in the " ~
"late 1980s. As a permissive license, it puts only very limited restriction " ~
"on reuse and has, therefore, high license compatibility.";
    menu.addItem("1", "MIT License", {
        showMessage("MIT", mit);
        runMenu();
    });

    auto apache = "The Apache License is a permissive free software license " ~
"written by the Apache Software Foundation. It allows users to use the " ~
"software for any purpose, to distribute it, to modify it, and to distribute " ~
"modified versions of the software under the terms of the license, without" ~
"concern for royalties.";
    menu.addItem("2", "Apache License", {
        showMessage("Apache", apache);
        runMenu();
    });

    auto gnu = "The GNU General Public License (GNU GPL or simply GPL) is a "~
"series of widely used free software licenses that guarantee end users the " ~
"freedom to run, study, share, and modify the software.";
    menu.addItem("3", "GNU GPL", {
        showMessage("GNU GPL", gnu);
        runMenu();
    });

    menu.addItem("-", "Exit from menu", {});

    menu.run();
}


void runCalendar() {
    auto calendar = new Calendar("Calendar", "Choose a day:");
    calendar.run();
    runInfoBox("You chose " ~ calendar.getDate().toISOExtString(), 2);
}


void runTimeBox() {
    auto timebox = new TimeBox("Time", "Choose a time:");
    timebox.run();
    runInfoBox("You chose " ~ timebox.getTime().toISOExtString(), 2);
}


void main() {
    Dialog.setBackTitle("Big sample");
    scope(exit) clearScreen();

    runMsgBox();
    runInputBox();
    runCheckList();
    runRadioList();
    runMenu();
    runCalendar();
    runTimeBox();

    runYesNo();
}

