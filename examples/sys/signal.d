#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.
 * This file is offered as-is, without any warranty.
 */

import std.stdio;
import amalthea.sys : addSignalHandler, Signal, getSignalDescription;
import amalthea.time : sleep;

bool end = false;


void onSignal(Signal signal) {
    writefln("\nHandling signal %s...", signal);
    writefln("Event: %s", getSignalDescription(signal));
    end = true;
}


void main() {
    addSignalHandler(Signal.SIGINT, &onSignal);
    while(!end) {
        writeln("Waiting...");
        sleep(1);
    }
    writeln("Quit...");
}

